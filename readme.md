# Addressable简介
Addressable是可寻址资源，是一种通过地址加载资产的方法，能够简化内容包创建、部署。和更新。一旦资产被标记为可寻址，就可以从任何地方调用可寻址资源，可以通过AssetReference加载单个可寻址资源，也可以通过自定义组标签加载多个可寻址资产。
# 操作教程
+ 安装addressable
  ![install](readmeImage/install.png)
  ![install2](readmeImage/install2.png)
+ 将prefab标记为可寻址对象
  ![markaddressable](readmeImage/markaddressable.png)
+ 选取为可寻址对象之后，就可以在Addressables窗口看到勾选了addressable的资产如下图:![defaultgroup](readmeImage/defaultgroup.png)
+ 简化名称
  ![simpleName](readmeImage/simpleName.png)
+ 打标签组
  ![label](readmeImage/label.png)
+ Play Mode Script选择Use Existing Build(require built groups)
  ![playmodescript](readmeImage/playmodescript.png)
+ Profiles
![profiles](readmeImage/profiles.png)
![ProfilePaths](readmeImage/ProfilePaths.png)
+ Build
![Build](readmeImage/build.png)
+ 载入代码
  ```c#
  var operateHandler = Addressables.LoadAssetsAsync<GameObject>(Label, OnLoadComplete);
  ```

  完整代码：

  ```c#
  using System.Collections;
  using System.Collections.Generic;
  using UnityEngine;
  using UnityEngine.AddressableAssets;
  using UnityEngine.ResourceManagement.AsyncOperations;
  using UnityEngine.UI;
  
  public class CharacterManager : MonoBehaviour
  {
      public string Label="default";
      public Transform LoadingPanelTrans;
      LoadingPanel loadingPanel;
      bool m_AssetsReady = false;
      public Transform ButtonPanelTrans;
      public Dictionary<string, Button> btnDic;
      public Dictionary<string, GameObject> objDic;
      public Transform spawnParent;
      private void Awake()
      {
          btnDic = new Dictionary<string, Button>();
          objDic = new Dictionary<string, GameObject>();
          for (int i=0;i< ButtonPanelTrans.childCount; i++)
          {
              Transform child = ButtonPanelTrans.GetChild(i);
              if (!btnDic.ContainsKey(child.name))
              {
                  Button btn = child.GetComponent<Button>();
                  btnDic.Add(child.name, btn);
                  btn.onClick.AddListener(() => { SpawnCharacter(btn.name); });
                  btn.interactable = false;
              }
          }
      }
      // Start is called before the first frame update
      IEnumerator Start()
      {
          if(LoadingPanelTrans!=null)
              loadingPanel = new LoadingPanel(LoadingPanelTrans);
          var operateHandler = Addressables.LoadAssetsAsync<GameObject>(Label, OnLoadComplete);//通过label载入
          operateHandler.Completed += LoadAddressables_Completed;
          while (!operateHandler.IsDone)
          {
              loadingPanel.ShowProgress(operateHandler.PercentComplete);//显示进度条
              Debug.Log(operateHandler.PercentComplete);//此处为载入进度
              yield return null;
          }
          yield return null;
      }
  
      private void OnLoadComplete(GameObject obj)
      {
          if(!objDic.ContainsKey(obj.name))
          objDic.Add(obj.name, obj);
          if(btnDic.ContainsKey(obj.name))
              btnDic[obj.name].interactable = true;
      }
  
      private void LoadAddressables_Completed(AsyncOperationHandle<IList<GameObject>> obj)
      {
          loadingPanel.Hide();//隐藏loading界面
      }
  
      public void SpawnCharacter(string characterName) {
          Debug.Log(characterName);
          Vector3 position = Random.insideUnitSphere * 5;
          position.Set(position.x, 0, position.z);
          if (objDic.ContainsKey(characterName))
              Instantiate<GameObject>(objDic[characterName],position,Quaternion.identity,spawnParent);
      }
      private void OnDestroy()
      {
  		btnDic.clear();
  		objDic.clear();
      }
  }
  
  ```
  用这种方式载入通过operateHander.PercentComplete可以获取下载进度，还有另一种载入的方式:

  ```c#
  using System.Collections.Generic;
  using UnityEngine;
  using UnityEngine.AddressableAssets; //TODO: Mention the use of this namespace
  using UnityEngine.ResourceManagement.AsyncOperations; // TODO: Mention that this is needed to do the async operations over the lists?
  
  public class CharacterManager : MonoBehaviour
  {
      public GameObject m_archerObject;
  
      public AssetReference m_ArcherObject;
  
      public List<AssetReference> m_Characters;
      bool m_AssetsReady = false;
      int m_ToLoadCount;
      int m_CharacterIndex = 0;
  
      Start is called before the first frame update
      void Start()
      {
          m_ToLoadCount = m_Characters.Count;
  
          foreach (var character in m_Characters)
          {
             character.LoadAssetAsync<GameObject>().Completed += OnCharacterAssetLoaded;
          }
      }
  
  
      public void SpawnCharacter(int characterType)
      {
          Instantiate(m_archerObject);
  
          m_ArcherObject.InstantiateAsync();
  
          if (m_AssetsReady)
          {
             Vector3 position = Random.insideUnitSphere * 5;
             position.Set(position.x, 0, position.z);
             m_Characters[characterType].InstantiateAsync(position, Quaternion.identity);
          }
      }
  
      void OnCharacterAssetLoaded(AsyncOperationHandle<GameObject> obj)
      {
         m_ToLoadCount--;
  
         if (m_ToLoadCount <= 0)
             m_AssetsReady = true;
      }
  
      private void OnDestroy() //TODO: Should we teach instantiate with game objects and then manually release?
      {
         foreach (var character in m_Characters)
         {
             character.ReleaseAsset();
         }
      }
  }
  
  ```
  这种方式也可以载入资源，但是打出apk会显示获取进度一直卡在50%（进度显示有问题，但是不影响下载） 
+ 打完包后的路径位于ServerData对应打包的平台目录下。本例是这个目录:ServerData\Android
+ 将ServerData目录底下的文件上传服务器即可，本例服务器路径如下:![server](readmeImage/server.png)
  自己配置远程服务器需要修改Profiles中对应远程服务器的地址，重新打包并上传自己的服务器
+ 更新资源：
![updatePreviousBuild](readmeImage/updatePreviousBuild.png)
+ 会弹出一个让你选的框
![路径如下](readmeImage/路径如下.png)
+ 打包apk
![buildapk](readmeImage/buildapk.png)

## 注意
在android资源打包addressable之后在Editor中运行呈现粉色，但实际打包app没有什么问题
[apk下载地址](https://gitee.com/sika0819/AddressableLearn/tree/build/Build/test.apk)