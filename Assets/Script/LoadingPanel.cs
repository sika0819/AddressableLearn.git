﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class LoadingPanel 
{
    Transform PanelTrans;
    public GameObject SliderBarGo;
    Slider slider;
    public GameObject CircleBarGo;
    Image progressBar;
    Text progressText;
    public LoadingPanel(Transform trans)
    {
        PanelTrans = trans;
        if (PanelTrans == null) {
            Debug.LogError("请选择载入进度条");
            return;
        }
        SliderBarGo = trans.Find("SliderBarGo").gameObject;
        if (SliderBarGo != null)
        {
            slider = SliderBarGo.GetComponent<Slider>();
        }
        else
        {
            Debug.LogError("未包含SliderBarGo物体");
        }
        CircleBarGo = trans.Find("CircleBarGo").gameObject;
        if (CircleBarGo != null)
        {
            progressBar = CircleBarGo.transform.Find("progressBar").GetComponent<Image>();
            progressText = CircleBarGo.transform.GetComponentInChildren<Text>();
        }
        else
        {
            Debug.LogError("CircleBarGo");
        }
    }
    public void Show()
    {
        
        PanelTrans.gameObject.SetActive(true);
    }
    public void Hide()
    {
        PanelTrans.gameObject.SetActive(false);
    }
    public void ShowProgress(float progress)
    {
        if (progress != 1)
        {
            Show();
            if (slider != null)
            {
                slider.value = progress;
            }
            else
            {
                Debug.LogError("未包含slider控件");
            }
            if (progressBar != null)
            {
                progressBar.fillAmount = progress;
            }
            else
            {
                Debug.LogError("未包含progressBar图片控件");
            }
            if (progressText != null)
            {
                progressText.text = string.Format("{0:F2}%", progress * 100);
            }
            else
            {
                Debug.LogError("未包含progressText文本控件");
            }
        }
        else
        {
            Hide();
        }
    }
}
