/*Auto-Generated File from VersionEditor 2020.01.16 17:32:59 */

namespace Assets.Scripts.VersionEditor {
	public static class VersionInformation {
		public new static string ToString() {
			return string.Format("{0}.{1}.{2}.{3}", Major, Minor, Revision, Build);
		}
		public const int Major = 1;
		public const int Minor = 0;
		public const int Revision = 0;
		public const string Build = @"0";

		public const string Title = "AddressableLearn";
		public const string Company = "SikaCompany";
	}
}

